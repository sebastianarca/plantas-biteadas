## Dependencias:

 - WiFiEsp by bportaluri
 - DHT sensor library by Adafruit
 - Adafruit Unified Sensor by Adafruit 
 - ArduinoJson by Benoit Blanchon 
 - esp8266_mdns by mrdunk
 - U8g2 by oliver 
 - LiquidCrystal Built-In by Arduino, Adafruit 

-------------------------------

En esta version solo hay un sensor DHT22 y el api solo devuelve informacion json.


Lista de mejoras:
 - Interactuar con reles
 - API para mandar acciones (POST)
 - Agregar sensor de temperatura
 - Agregar sensor de humedad tierra
 - En la configuracion web agregar opcion de Gateway
 - Recopilar informacion y mandarla via JSON al gateway/registry
 - mDNS con codigo alfanumerico aleatorio onda c4aa3bb2cc.local almacenado en memoria interna
 


## API REST 

GET ip:80/

**Interfaz que debe tener las publicaciones del ESP866**

 ```
 {
    'id': String,
    'titulo': String,
    'schema': {
        'sensores': {
            'sensor_label_1': 'Sensor Nombre 1',
            'sensor_label_2': 'Sensor Nombre 2',
        },
        'acciones': 4
    },
    'data': {
        'sensor_label_1': float,
        'sensor_label_2': float,
    },
    acciones: {
        0: true,
        1: false,
        2: false,
        3: false,
    }
 }
 ```
 -------------- Ejemplo 2222 ----------- TODO hacer API -------
 ```
 {
   "id": "locoto",
   "titulo": "Descripcion del cultivo",
   "schema": {
       "sensores": {"hum":"humedad", "temp":"temperatura", "hum_tierra": "humedad"},
       "acciones": 4,
    },
    "data": {
       "hum": 50.75,
       "hum_tierra": 80.4,
       "temp": 20
    },
    "acciones":{
       "0": "Bomba Agua",
       "1": "Ventilador",
       "2": "Luces"
    }
 }
 ```
